// Array function series of Macaroni -*- C++ -*-

// Copyright (c) 2020 by Andrés Galván
//     
// Macaroni 0.0.1 - Basic functions to make arrays process faster.
//

/** @file macaroni
 *  This is a Standard C++ Library header.
*/

#include "macaroni.hh"


// TODO Ask to user the size of vector
void askVector(char fillingMethod)
{
    /* Ask for the size of vector and receive a character
      as argument that could be 'r' or 'm' as filling methods.
      where r is random filling and m is manually filling.
      * Instance: askVector('r');
        |
        └─> This will generate a vector with random values from user's ranges.

      * Instance: askVector('M');
        |
        └─> This will ask to user every element of the array.
    */
    bool fillingLoop;

    std::string size; // Input to be valide to for only numbers
    int sizeNum; // Number to be used as the size of the vector

    fillingMethod = tolower(fillingMethod);

    do
    {
        std::cout << "Ingrese el tamaño del vector: ";
        std::cin >> size;
        fillingLoop = inputNumbers('p', size);
        if (fillingLoop) continue;

        sizeNum = std::stoi(size);
        std::cout << std::endl;

        if (fillingMethod == 'm') fillVectorManually(sizeNum);
        else if (fillingMethod == 'r') askVectorLimits(sizeNum);
        else { errorIdentifier("askVector('x')"); errorMsg(9); exit(0); }
        
    } while (fillingLoop);
    
}


// TODO Fill the vector by asking the values to the user.
void fillVectorManually(int sizeNum)
{
    /* This function is called if the askVector function is called
    with a 'm' as a paramather.
    It will be ask to the user every value of the array and check
    that only numbers are entered, in case of a non-numeric value
    is introduced a error msg will be shown and will ask to the 
    user to introduce tha value again.    
    */

    /* A additional variable is used for verify that the input is only
    numbers, then is passed to the array as a number.*/
    std::string verifyValue;//      /
    float array[1000]; //<──────── *

    bool fillingLoop;

    for (int i = 0; i < sizeNum; i++)
    {
        std::cout << "Ingrese el valor "<< i+1 << ": ";
        std::cin >> verifyValue; // Fill the string's array
        fillingLoop = inputNumbers('a', verifyValue); // Validate only numbers

        if (fillingLoop)
        {
            i--;
            continue;
        }
        array[i] = std::stof(verifyValue); // Convert to numbers
    }

    displayVector(array, sizeNum); // Show the vector
}


// TODO Ask the user the limits of the random array.
void askVectorLimits(int sizeNum)
{
    /* This function is called if the askVector function is called
    with a 'r' as a paramather.
    In this function will ask to the user the random vector limits
    and validate there's only numbers in the input.
    This values will be used to created a random vector within the
    limits.
    */
    std::string range1, range2;
    int rangeNum1, rangeNum2;

    bool fillingLoop;

    do
    {
        // Ask for the number's range
        std::cout << "\nIngrese el rango de números.\n";

        std::cout << "\nDesde: "; std::cin >> range1; // Use strings to validate
        fillingLoop = inputNumbers('p', range1);
        if (fillingLoop) continue; // Continue asking till the value be a number

        std::cout << "Hasta: "; std::cin >> range2; // Use strings to validate
        fillingLoop = inputNumbers('p', range2);

        // Convert into numbers
        rangeNum1 = std::stoi(range1);
        rangeNum2 = std::stoi(range2);

        if (rangeNum1 > rangeNum2) // Validate that first range is the lower than second
        {
            errorIdentifier(range2);
            errorMsg(8); // Print Error in case that happen
        }

    } while (fillingLoop || range1 > range2);

    fillVectorRandomly(sizeNum, rangeNum1, rangeNum2);
}


// TODO Fill the vector by random numbers within the ranges from the user.
void fillVectorRandomly(int sizeNum, int rangeNum1, int rangeNum2)
{
    /* Use this function to don't worry about the content of your array,
    You only need to introduce the size of the array, the upper limit and
    the lower limit, of course you can ask that to the user.
      * Instance: fillVectorRandomly(5, 1, 5);
        |
        └─> This will generate a vector with random values like this:
                [1, 4, 5, 1, 3]
    */
    float array[1000];
    int aleatory;
    for (int i = 0; i < sizeNum; i++)
    {
        // Gen the vector with users range
        aleatory = rangeNum1 + rand() % ((rangeNum2 + 1) - rangeNum1);
        array[i] = aleatory;
    }
    displayVector(array, sizeNum);
    std::cout << std::endl;
}


// TODO Display the vector horizontally.
void displayVector(float array[], int sizeNum)
{
    /* If you already have a vector and want to display it, you
    can call this function and it'll go over your vector to display it
    horizontally, separated with comas.
    */
    int comas = 0;
    
    std::cout << "\n\nVECTOR.\n"; // Print the title
    for (int i = 0; i < sizeNum; i++)
    {
        std::cout << array[i]; // Print the vector

        comas++;
        // Print a coma until the penultimate value
        if (comas < sizeNum) std::cout << ", ";
        // If it gets the final term of the vector print a point
        else  std::cout << ".";
    }
}


//TODO Searching methods
float getElementByPosition(float array[], int size)
{
    bool loopQuestion;

    std::string positionIsNumeric;
    int position;

    std::cout << "\nBuscar un elemento por su posición.\n\n";
    do
    {
        std::cout << "Posición: "; std::cin >> positionIsNumeric;
        loopQuestion = inputNumbers('p', positionIsNumeric);

        if (loopQuestion) continue;
        position = std::stoi(positionIsNumeric);

        if (position >= size) {
            errorIdentifier(positionIsNumeric);
            errorMsgPersonalized("Error. Posición fuera de rango.");
        }

    } while (loopQuestion || position >= size); 

    return array[position];
}


//TODO 
void getElementByValue(float array[], int size)
{
    bool loopQuestion;
    int found = 0;

    std::string valueIsNumeric;
    int value;

    std::cout << "\nBuscar un elemento por su posición.\n\n";
    do
    {
        std::cout << "Valor: "; std::cin >> valueIsNumeric;
        loopQuestion = inputNumbers('p', valueIsNumeric);

        if (loopQuestion) continue;
        value = std::stoi(valueIsNumeric);

        for (int i = 0; i < size; i++)
        {
            if (value == array[i])
            {
                std::cout << value << " encontrado en la posición [" << i << "].";
                found++;
            }

        }

    } while (loopQuestion); 
    if (found == 0)
    {
        std::cout << value << " no encontrado.";
        getElementByValue(array, size);
    }
    std::cout << "\n";
}


// TODO Bubble sort
void bubbleSort(float array[], int sizeNum)
{
    int pos, temp;

    for (int i = 0; i < sizeNum; i++)
    {
        pos = i;
        temp = array[i];

        while ((pos > 0) && (array[pos - 1] > temp))
        {
            array[pos] = array[pos - 1];
            pos--;
        }
        array[pos] = temp;
    }    

    displayVector(array, sizeNum);
    std::cout << "\n";
}
