// Macaroni functions -*- C++ -*-

// Copyright (c) 2020 by Andrés Galván
//     
// Macaroni 0.0.1 - Basic functions to make console programs faster.
//                      
// Macaroni is a student usage oriented micro-library, that is a C++
// version of Macarena Module made for python. This simple Library is
// for basic side functions that allow the developer (student) to 
// focus on the logic of the main process.

// This library comes WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception,
// version 3.1, as published by the Free Software Foundation.


/** @file macaroni
 *  This is a Standard C++ Library header.
 */

#ifndef _MACARONI
#define _MACARONI

// Functions of basics.cpp
void clearConsole();
void errorMsgPersonalized(std::string message);
void errorMsg(int errorType);
void errorIdentifier(std::string errorID);
void printTitle(std::string title, int centered, bool inCaps);
int isInteger(std::string evaluate);
int isFloat(std::string evaluate);
char *removeSpaces(char *str);
void repeatProgram(int main_function());
bool inputName(char *name);
bool inputNumbers(char numbersRange, std::string evaluate);

// Functions of vectors.cpp
void askVector(char fillingMethod);
void fillVectorManually(int sizeNum);
void askVectorLimits(int sizeNum);
void fillVectorRandomly(int sizeNum, int rangeNum1, int rangeNum2);
void displayVector(float array[], int sizeNum);
float getElementByPosition(float array[], int size);
void getElementByValue(float array[], int size);
void bubbleSort(float array[], int sizeNum);

//Functions of matrix.cpp

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "basics.cpp"
#include "vectors.cpp"
#include "matrix.cpp"
#endif // !_MACARONI